'use strict';

// ui components
var $textarea;

// webrtc stuff
var maxRetries = 5;
var retries = 0;
var connection;
var madeOffer = false;
var sentOffer = false;
var sendChannel;
var receiveChannel;
var payload = {};
payload.candidates = [];

$(document).ready(function() {
  $textarea = $('#textarea');
  createConnection();
  var offer = findGetParameter('offer');
  if(offer) {
    createAnswer(offer);
  }
  else {
    createOffer();
    $('#next-step').click(nextStep);
  }
});

function findGetParameter(parameterName) {
  var result = null,
    tmp = [];
  location.search
    .substr(1)
    .split("&")
    .forEach(function (item) {
      tmp = item.split("=");
      if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    });
  return result;
}

function createConnection() {
  var configuration = { iceServers: [{ urls: "stun:stun.l.google.com:19302" }]};
  var servers = null;
  var pcConstraint = null;
  var dataConstraint = null;

  connection = new RTCPeerConnection(configuration);
  sendChannel = connection.createDataChannel('sendDataChannel', dataConstraint);
  connection.onicecandidate = iceCallback;
  connection.oniceconnectionstatechange = iceStateChangeCallback;
  sendChannel.onopen = onSendChannelStateChange;
  sendChannel.onclose = onSendChannelStateChange;
  connection.ondatachannel = receiveChannelCallback;
}

function createOffer() {
  madeOffer = true;
  connection.createOffer().then(
    gotDescription,
    function(error) { console.log('failed to create offer: ' + error.toString()); }
  );
}


function gotDescription(desc) {
  console.log('got description');
  connection.setLocalDescription(desc);
  payload.desc = desc;
  updateLocalPayload();
}


function onSendChannelStateChange() {
  var readyState = sendChannel.readyState;
  console.log('send channel state changed to: ' + readyState);
  if(readyState === 'open') {
    gotoChat();
  }
  else{
  }
}

function receiveChannelCallback(event) {
  receiveChannel = event.channel;
  receiveChannel.onmessage = onReceiveMessageCallback;
  receiveChannel.onopen = onReceiveChannelStateChange;
  receiveChannel.onclose = onReceiveChannelStateChange;
}

function onReceiveMessageCallback(event) {
  var data = JSON.parse(event.data);
  data.received = true;
  console.log('received message: ' + data);
  displayMessage(data);
}

function displayMessage(data) {
  var who = (data.received) ? "them" : "me";
  var time = escapeHtml(data.time);
  var dn = escapeHtml(data.dn);
  var message = fleuParse(data.message);
  var $messageHtml = $('<div class="message ' + who + '">'
  + '<div class="data">'
  + '<div class="dn">' + dn + '</div>'
  + '<div class="time">' + time + '</div>'
  + '</div>'
  + '<div class="text">' + message + '</div>'
  + '</div>');
  $('#chat-output').append($messageHtml);
  $messageHtml.find('pre code').each(function(i, block) {
    var contents = block.innerHTML;
    contents = contents.replace(/<br>/g, "\n");
    block.innerHTML = contents;
    hljs.highlightBlock(block);
  });
  document.querySelector("#chat-output").scrollTo(0, document.querySelector("#chat-output").scrollHeight);
}

function onReceiveChannelStateChange() {
  var readyState = receiveChannel.readyState;
  console.log('receive channel state changed to: ' + readyState);
}

function iceStateChangeCallback(event) {
  var state = connection.iceConnectionState;
  console.log('ice state change: ' + state);
  if(state === 'failed' && retries < maxRetries) {
    createConnection();
    processPayload();
    retries ++;
    console.log(
      'retrying ice connection! ' 
      + (maxRetries - retries) 
      + ' retries remain...');
  }
}

function iceCallback(event) {
  console.log('local ice callback');
  if(event.candidate) {
    console.log('found candidate: ');
    console.log(event.candidate);
    payload.candidates.push(event.candidate);
    updateLocalPayload();
  }
}

function updateLocalPayload() {
  var jsonPayload = JSON.stringify(payload);
  const lib = JsonUrl('lzma');
  lib.compress(jsonPayload).then(output => { 
    if(madeOffer){
      var getUrl = window.location;
      var baseUrl = getUrl.protocol + '//' + getUrl.host
      $textarea.val(baseUrl + '/brat?offer=' + output); 
    }
    else {
      $textarea.val(output);
    }
  });
}


function nextStep(event) {
  if(sentOffer === false) {
    $('#textarea').val('');
    $('#step').text('Step 2: Paste their answer.');
    $('#description').text(
      'Before the connection can be made, the other '
      + 'party must send you their response. (It looks '
      + 'like a big garbled bunch of text.) Paste it '
      + 'in the box below, then hit next. The connection '
      + 'will be established, then you\'ll be redirected '
      + 'to the chat window.');
    sentOffer = true;
  }
  else {
    decompressPayload($('#textarea').val());
  }
}


// answer section

function createAnswer(offer) {
  $('#step').text('You have been invited to chat!');
  $('#description').text(
    'All you need to do is copy the text below, and '
    + 'send it to whoever invited you to chat. '
    + 'Once they\'ve finished setting up, you will '
    + 'automatically be redirected to the chat window.');
  $('#next-step').css('visibility', 'hidden');
  decompressPayload(offer);
}

function decompressPayload(offer) {
  var rpayload = offer
  const lib = JsonUrl('lzma');
  lib.decompress(rpayload).then(output => { processPayload(output); });
}

function processPayload(offer) {
  var rpayload = offer

  rpayload = JSON.parse(rpayload);

  connection.setRemoteDescription(rpayload.desc);
  if(!madeOffer) {
    connection.createAnswer().then(
      gotDescription,
      function(error) { console.log('failed to create offer: ' + error.toString()); }
    );
  }
  for(var i = 0; i < rpayload.candidates.length; i++){
    connection.addIceCandidate(rpayload.candidates[i]).then(
      function() { console.log('successfully added ice candidate') },
      function() { console.log('failed to add ice candidate') }
    );
  }
}

function gotoChat() {
  $('#site').html(
    '<div id="chat-area">'
    + '<input id="display-name" type="text" placeholder="enter your display name" value="anonymous"></input>'
    + '<div id="chat-output" class="fleuview"></div>'
    + '<textarea id="chat-input" class="fleutext" placeholder="type your message, then hit enter to send!"></textarea>'
    + '</div>');
  $('#chat-input').on('keypress', handleChatInput);
}

function handleChatInput(event) {
  if(event.which === 13 && event.shiftKey === false) {
    event.preventDefault();
    event.stopPropagation();
    sendMessage();
  }
}

function sendMessage() {
  var time = (new Date()).toLocaleTimeString();
  var message = { time: time, dn: $('#display-name').val(), message: $('#chat-input').val() };
  message.dn = message.dn ? message.dn : 'anonymous';
  var json = JSON.stringify(message);
  sendChannel.send(json);
  displayMessage(message);
  $('#chat-input').val('');
}


