
class Basecon < Microcon

  def view_helper
    @view_helper ||= {
      title: "#{self.class.name[0...-3]}",
      stylesheets: "<link href='/#{self.class.name}/style.css' type='text/css' rel='stylesheet'>",
      javascripts: "<script src='/#{self.class.name}/main.js' type='text/javascript'></script>"
    }
  end

end
