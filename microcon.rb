
class Microcon < Sinatra::Base

  @@descendants = []

  def self.inherited(subclass)
    @@descendants << subclass
    super
  end

  def self.descendants
    @@descendants
  end

end
